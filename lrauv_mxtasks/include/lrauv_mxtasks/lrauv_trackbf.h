#ifndef MX_LRAUV_TRACKBF_H
#define MX_LRAUV_TRACKBF_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_libtrackline/Trackline.h>
//#include <lrauv_msgs/LrauvMxGoal.h>
#include <ds_control_msgs/GoalLegLatLon.h>
#include <ds_control_msgs/BottomFollow1D.h>
#include "std_msgs/Float32.h"

namespace lrauv_mxtasks {

class TaskTracklineBf : public ds_mx::MxPrimitiveTask {
 public:
  TaskTracklineBf();

  void init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) override;
  void init_ros(ros::NodeHandle& nh) override;
  bool validate() const override;

  void onStart(const ds_nav_msgs::NavState& state) override;
  void getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) const override;

  void resetTimeout(const ros::Time& now) override;

  //lrauv_msgs::LrauvMxGoal buildCmdMessage(const ds_nav_msgs::NavState& state) const;
  ds_control_msgs::GoalLegLatLon buildCmdMessage(const ds_nav_msgs::NavState& state) const;

 protected:
  ds_mx::GeoPointParam start_pt;
  ds_mx::GeoPointParam end_pt;

  ds_mx::DoubleParam speed;
  ds_mx::DoubleParam altitude;
  ds_mx::DoubleParam depth_floor;

  // extra paramaters because why not
  ds_mx::DoubleParam envelope;
  ds_mx::DoubleParam gain;
  ds_mx::DoubleParam depthvel;
  ds_mx::DoubleParam depthacc;
  ds_mx::DoubleParam min_speed;
  ds_mx::DoubleParam alarm_timeout;

  /// \brief The final timeout is predicted time * multiplier + increment
  ds_mx::DoubleParam timeout_multiplier;

  /// \brief The final timeout is predicted time * multiplier + increment
  ds_mx::DoubleParam timeout_increment;

  ds_trackline::Trackline trackline;

  ros::Publisher cmd_pub_;
  ros::Publisher rpm_pub_;
  ros::Publisher bf_pub_;

  /// \brief This handler will cause this task to FAIL if its received
  ds_mx::EventHandler cancel;

  void parameterChanged(const ds_nav_msgs::NavState& state) override;
  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState& state) override;
  void updateTrackline();
  bool event_detected(ds_mx_msgs::MxEvent event);
};

} // namespace lrauv_mxtasks

#endif //MX_LRAUV_TRACKBF_H
